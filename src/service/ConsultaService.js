import axios from 'axios';

export class ConsultaService {
   
    baseUrl = "http://192.168.0.84:8081/api/ficha/";

    getAll(){
        return axios.get(this.baseUrl + "all").then(res => res.data);
    }

    getFicha(fichaId){
        return axios.get(this.baseUrl + "find/" + fichaId);
    }
    
    save(ficha){
        return axios.post(this.baseUrl + "save", ficha).then(res => res.data);
    }
    
    saveImg(data){
        return axios.post(this.baseUrl + "upload", data);
    }

    delete(fichaId) {
        return axios.delete(this.baseUrl+ "delete/" + fichaId);
    }

    update(ficha, fichaId){
        return axios.put(this.baseUrl + fichaId, ficha);
    }
}
