import React from 'react';
import ReactDOM from 'react-dom';
import './index.css'; 
import AppRouter from './AppRouter';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import store from './redux/store';

//Estilos Bootstrap
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'; // Archivo CSS de Bootstrap
import '../node_modules/bootstrap/dist/js/bootstrap.min.js'; // Archivo Javascript de Bootstrap

import '../node_modules/datatables.net-dt/js/dataTables.dataTables';
import '../node_modules/datatables.net-dt/css/jquery.dataTables.min.css';
import '../node_modules/jquery/dist/jquery.min.js';


ReactDOM.render(
	<Provider store={store}>
	<React.StrictMode>
	  <AppRouter />
	</React.StrictMode>
	</Provider>,
	document.getElementById('root')
  );

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
