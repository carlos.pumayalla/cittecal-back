import React from 'react';
import TituloMostrar from './tituloMostrar/TituloMostrar';
import FormMostrar from './formMostrar/FormMostrar';
import MenuLateral from '../acoples/menuLaterar/MenuLaterar';

class MostrarFicha extends React.Component {
    render(){
        return(
            <>
             <MenuLateral />
            <TituloMostrar/> 
			<main role="main" class="">
                <div class="">
				    <FormMostrar />
                </div>	
	  		</main>
	  		</>
        )
    }
}
export default MostrarFicha;