import React, {useState} from 'react';
import Footer from '../acoples/footer/Footer';
import Menu from '../acoples/menu/Menu';
import Inicio from './inicio/Inicio';
import MenuLateral from '../acoples/menuLaterar/MenuLaterar';
import {fetchUserData} from '../../api/authenticationService';
import { useDispatch } from 'react-redux';
import { faTable } from '@fortawesome/free-solid-svg-icons';

export const Home =(props)=> {

    const dispatch=useDispatch();
    const [loading,setLoading]=useState(false);
    const [data,setData]=useState({});

    React.useEffect(()=>{
        fetchUserData().then((response)=>{
            setData(response.data);
        }).catch((e)=>{
            localStorage.clear();
            props.history.push('/');
        })
    },[])

    
        return(
            <>
                <MenuLateral/> 
                <Menu/>
                <main role="main" class="">
                    <Inicio />
                </main>
	  		</>
        )
    
}
export default Home;





