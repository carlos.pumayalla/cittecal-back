import React, { useState } from 'react'
import homefondo from '../../../assets/img/homefondo1.png';
import itp from '../../../assets/img/ITP.png';
import produce from '../../../assets/img/PRODUCE.png';
import '../../../assets/styles/divs.css';


const Inicio = () => {
    return (
        
        <div class="row sa" style={{backgroundColor: '#E3E2D8'}}>
                <div class="col-7">
                    <img src={homefondo} className="img-fluid float-center"></img>
                </div>
                <div class="col-5" >
                    <div  style={{marginTop:'70px', display: 'flex', alignContent:'center', marginLeft:'50px'}}>
                        <img src={produce} height='8%' width='45%' style={{marginTop:'32px'}}/>
                        <img src={itp} height='20%' width='40%' />
                    </div>
                    <div style={{display:'block', marginTop:'95px'}}>
                        <div style={{backgroundColor:'#b72727', width:'330px', marginLeft:'55px'}}>
                            <p style={{
                                marginLeft: '10px',
                                fontSize:30,
                                color:'white',
                                fontWeight: 'bold',
                                
                            }}>Impulsamos al sector</p>
                        </div>
                        <div style={{backgroundColor:'#b72727', width:'305px',  marginLeft:'55px'}}>
                            <p style={{
                                marginLeft: '10px',
                                fontSize:30,
                                color:'white',
                                fontWeight: 'bold',
                            }}>cuero y calzado con</p>
                        </div>
                        <div style={{backgroundColor:'#b72727', width:'365px',  marginLeft:'55px'}}>
                            <p style={{
                                marginLeft: '10px',
                                fontSize:30,
                                color:'white',
                                fontWeight: 'bold',
                            }}>innovación y tecnología</p>
                        </div>
                    </div>
                </div>
        </div>
    )
}

export default Inicio
