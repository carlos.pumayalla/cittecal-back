import React from 'react';
import './../../../assets/styles/divs.css';

class TituloLista extends React.Component {
    render(){
        return(
            <div id="divTitulo" style={{backgroundColor:'#5E5446'}} class="px-5 py-1 text-center">
                <h2 class="py" style={{ color: "#FFFFFF", fontFamily:"sans-serif" }} ><b>Fichas Técnicas</b></h2>
            </div>
        )
    }
}
export default TituloLista;