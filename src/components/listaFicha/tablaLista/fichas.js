import axios from "axios";

const ficha = {}

ficha.getFichas = async () => {
    const res = await axios.get("http://192.168.0.84:8081/api/ficha/all")
        .then(resp => {
            return resp.data;
        })

    return res;
}

export default ficha;