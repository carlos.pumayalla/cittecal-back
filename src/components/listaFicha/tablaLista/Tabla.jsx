import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faClipboardList, faFilePdf, faRedo, faTrash, faImage} from "@fortawesome/free-solid-svg-icons";
import './../../../assets/styles/divs.css';
import { Link } from 'react-router-dom';
import {fetchUserData} from '../../../api/authenticationService';
import ficha from './fichas';
import { ConsultaService } from '../../../service/ConsultaService';
import axios from 'axios';

export default function Tabla(){
    const [listFichas, setListFichas] = useState([])
    const [search, setSearch] = useState("")
    const [currentPage, setCurrentPage] = useState(0)
    const [data,setData]=useState({});

    const obtenerFichas = async () => {
        const res = await ficha.getFichas();
        setListFichas(res)
    }

    const ListFiltered = () => {
        if (search){
            const filtered = listFichas.filter((ficha) => ficha.fichaId.toString().includes(search)|| ficha.linea.toString().includes(search)|| ficha.estilo.toString().includes(search))
            
            return filtered.slice(currentPage, currentPage + 5);
        } else {
            return listFichas.slice(currentPage, currentPage + 5)
        }
    }


    const nextPage = () => {
        const cant = listFichas.filter(ficha => ficha.id.toString().includes(search)).length;

        if (cant > currentPage) {
            setCurrentPage(currentPage + 5)
        }
    }

    const prevPage = () => {
        if (currentPage > 0) {
            setCurrentPage(currentPage - 5)
        }
    }

    const buscar = (e) =>{
        setSearch(e.target.value);
        
    }

    useEffect(() => {
        obtenerFichas();
        fetchUserData().then((response)=>{
            setData(response.data);
        })}, []);

    return(
        <div style={{ backgroundColor:'#E3E2D8'}} className="container-fluid vh-100"><br/>
            <div id="divTabla" style={{ backgroundColor:'#FFFFFF'}} class="container px-1 py-5">

                <div class="d-flex container text-center">
                    <div class="p-2 container">
                        <div>
                            <input type="text" class="form-control w-50" onChange={buscar} placeholder="Buscar Fichas Técnicas"/>
                        </div>
                    </div>
                    <div class="container-fluid ml-auto p-2">
                        {data && data.roles && data.roles.filter(value => value.roleCode==='ADMIN').length>0 &&
                        <div class="botonNuevo">
                            <Link to="/nuevaFicha" class="btn" style={{backgroundColor:'#373b3e', color:'#ffffff'}}>Nueva Ficha Técnica</Link>
                        </div>}
                    </div>
                </div><br/>
                <div class="container px-4">
                    <table class="table" id="tablaPag">
                        <thead class="table-dark text-center">
                            <tr>
                                <th scope="col">CODIGO</th>
                                <th scope="col">LINEA</th>
                                <th scope="col">SERIE</th>
                                <th scope="col">ALT. TACO</th>
                                <th scope="col">COLOR</th>
                                <th scope="col">ESTILO</th>
                                <th scope="col">HORMA</th>
                                <th scope="col">PLANTA</th>
                                <th scope="col">OPCIONES</th>
                            </tr>
                        </thead>
                        <tbody class="text-center" style={{backgroundColor:'#FFFFFF'}}>
                            {ListFiltered().map((ficha) => {
                                return(
                                    <tr key={ficha.id}>
                                        <td>{ficha.fichaId}</td>
                                        <td>{ficha.linea}</td>
                                        <td>{ficha.serie}</td>
                                        <td>{ficha.alTaco}</td>
                                        <td>{ficha.color}</td>
                                        <td>{ficha.estilo}</td>
                                        <td>{ficha.codHorma}</td>
                                        <td>{ficha.codPlanta}</td>
                                        <td>
                                            <Link to={`mostrarFicha/${ficha.id}`}>
                                                <button type="button" class="btn" style={{backgroundColor:'#FB9333'}}><FontAwesomeIcon icon={faClipboardList}/></button>
                                            </Link>
                                            {" "}
                                            {data && data.roles && data.roles.filter(value => value.roleCode==='ADMIN').length>0 &&
                                            <Link to={`editarFicha/${ficha.id}`}>
                                            <button type="button" class="btn" style={{backgroundColor:'#F9D620'}}><FontAwesomeIcon icon={faEdit}/></button>
                                            </Link>}
                                            {" "}
                                            {data && data.roles && data.roles.filter(value => value.roleCode==='ADMIN').length>0 &&
                                            <button type="button" class="btn" style={{backgroundColor:'#FF0000'}} onClick={()=>{
                                                if(window.confirm("¿Estás seguro de eliminar esta ficha?")){
                                                axios.delete('http://192.168.0.84:8081/api/ficha/delete/'+ficha.id)
                                            }}}><FontAwesomeIcon icon={faTrash}/></button>}
                                            {" "}
                                            {data && data.roles && data.roles.filter(value => value.roleCode==='ADMIN').length>0 &&
                                            <Link to={`img/${ficha.id}`}>
                                                <button type="button" class="btn" style={{backgroundColor:'#28B424'}}><FontAwesomeIcon icon={faImage}/></button>
                                            </Link>}
                                        </td>
                                    </tr>
                                );      
                            })}
                        </tbody>
                    </table>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            <li class="page-item">
                            <a className="btn" style={{backgroundColor:'#373b3e', color: '#FFFFFF'}} onClick={prevPage} >Anterior</a>
                            </li>
                            <li><a style={{color: '#FFFFFF'}}>nume</a></li>
                            <li class="page-item">
                            <a className="btn" style={{backgroundColor:'#373b3e', color: '#FFFFFF'}} onClick={nextPage} >Siguente</a>
                            </li>
                        </ul>
                    </nav>                                                           
                </div><br/>
            </div>
        </div>  
    )
}
