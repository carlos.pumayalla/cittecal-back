import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import logolat from '../../../assets/img/logolat.png';

const MenuLaterar = () => {
    const [ mostrar, setMostrar ] = useState(false);

    const mostrarMenu = () => {
        setMostrar(!mostrar);
    }

    return (
        <div>
            <div id="mostrar-nav" className="body" onClick={mostrarMenu}></div>
                <nav className={`nav-lat ${mostrar && "mostrar"}`}>
                <div id="cerrar-nav" onClick={mostrarMenu}></div>
                <div>
                    <img src={logolat} className="logo"></img>
                </div>
                    <ul className="menu navp">
                        <li><Link to="/home" style={{color: 'black'}}><b>Inicio</b></Link></li>
                        <li><Link to="/fichas" style={{color: 'black'}}><b>Ficha Tecnica</b></Link></li>
                        <li><Link to="/home" style={{color: 'black'}}><b>Servicios</b></Link></li>
                    </ul>
                </nav>
        </div>
    )
}

export default MenuLaterar
