import React, {useState} from 'react';
import {Navbar, Nav, NavDropdown, Container} from 'react-bootstrap';
import { withRouter } from 'react-router-dom';

export const Menu =(props)=> {

    const logOut=()=>{

        localStorage.clear();
    }

  
  	return (
          <div style={{backgroundColor:'#5E5446'}} class="container-fluid">
            <Navbar style={{backgroundColor:'#5E5446'}}  variant="dark">
                <Container class="row" style={{height:"30px"}}>
                    <div class="col-11"></div>
                    <Navbar.Collapse class="col-1">
                        <Nav className="me-auto">
                            <NavDropdown  title="Mi Cuenta">
                            {/* <NavDropdown.Item href="/#/miCuenta">Mi Cuenta</NavDropdown.Item>
                            <NavDropdown.Divider /> */}
                            <NavDropdown.Item onClick={() =>logOut()} href='/'>Cerrar Sesión</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
  	)
  
}
export default withRouter(Menu);

