import React from 'react';
import TtituloImg from './tituloImg/TituloImg';
import FormImg from './formImg/FormImg';
import MenuLateral from '../acoples/menuLaterar/MenuLaterar';

class SetImg extends React.Component {
    render(){
        return(
            <>
            <MenuLateral />
            <TtituloImg/> 
			<main role="main" class="">
                <div class="">
				    <FormImg />
                </div>	
	  		</main>
	  		</>
        )
    }
}
export default SetImg;