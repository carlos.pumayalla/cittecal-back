import React, { useState, useEffect } from 'react';
import './../../../assets/styles/nav-vertical.css';
import './../../../assets/styles/divs.css';
import $ from 'jquery';
import { Link } from 'react-router-dom';
import {useParams} from 'react-router-dom'
import axios from 'axios'

window.jQuery = $;

export default function FormImg()  {

    const [fichae, setFichae] = useState([])
    const {id} = useParams()
    const [file,setFile]=useState([])
    const [file2,setFile2]=useState([])
    const [file3,setFile3]=useState([])
    const [file4,setFile4]=useState([])
    const [state, setState]=useState({
        ficha: {
        id: null,
        fichaId: null,
        linea: null,
        serie: null,
        alTaco: null,
        color: null,
        estilo: null,
        codHorma: null,
        codPlanta: null,
        c1Tipo: null,
        c1Color: null,
        c2Tipo: null,
        c2Color: null,
        c3Tipo: null,
        c3Color: null,
        f1Tipo: null,
        f1Color: null,
        f2Tipo: null,
        f2Color: null,
        h1Tipo: null,
        h1Numero: null,
        h1Color: null,
        h2Tipo: null,
        h2Numero: null,
        h2Color: null,
        h3Tipo: null,
        h3Numero: null,
        h3Color: null,
        acc1Detalle: null,
        acc1Material: null,
        acc1Color: null,
        acc2Detalle: null,
        acc2Material: null,
        acc2Color: null,
        acc3Detalle: null,
        acc3Material: null,
        acc3Color: null,
        cierreDetalle: null,
        cierreMaterial: null,
        cierreColor: null,
        pullerDetalle: null,
        pullerMaterial: null,
        pullerColor: null,
        punteraMaterial: null,
        contraMaterial: null,
        p1Material: null,
        p1Color: null,
        p1Forrado: null,
        p2Material: null,
        p2Color: null,
        p2Forrado: null,
        plataformaMaterial: null,
        plataformaColor: null,
        plataformaForrado: null,
        tacoMaterial: null,
        tacoColor: null,
        tacoForrado: null,
        plantillaMaterial: null,
        plantillaColor: null,
        acolcheMaterial: null,
        h4Tipo: null,
        h4Numero: null,
        h4Color: null,
        costuraTipo: null,
        selloMarca: null,
        selloTipo: null,
        selloMaterial: null,
        cueritoMarca: null,
        cueritoTipo: null,
        cueritoMaterial: null,
        hantagMarca: null,
        img1: null,
        img2: null,
        img3: null,
        img4: null
    }})

    useEffect(() => {
        obtenerFicha();
    }, []);

    const obtenerFicha = async() => {
        const data = await fetch(`http://192.168.0.84:8081/api/ficha/find/${id}`)
        const ficha = await data.json()
        setFichae(ficha)

        setState({
            ficha:{
                fichaId: ficha.fichaId,
                linea: ficha.linea,
                serie: ficha.serie,
                alTaco: ficha.alTaco,
                color: ficha.color,
                estilo: ficha.estilo,
                codHorma: ficha.codHorma,
                codPlanta: ficha.codPlanta,
                c1Tipo: ficha.c1Tipo,
                c1Color: ficha.c1Color,
                c2Tipo: ficha.c2Tipo,
                c2Color: ficha.c2Color,
                c3Tipo: ficha.c3Tipo,
                c3Color: ficha.c3Color,
                f1Tipo: ficha.f1Tipo,
                f1Color: ficha.f1Color,
                f2Tipo: ficha.f2Tipo,
                f2Color: ficha.f2Color,
                h1Tipo: ficha.h1Tipo,
                h1Numero: ficha.h1Numero,
                h1Color: ficha.h1Color,
                h2Tipo: ficha.h2Tipo,
                h2Numero: ficha.h2Numero,
                h2Color: ficha.h2Color,
                h3Tipo: ficha.h3Tipo,
                h3Numero: ficha.h3Numero,
                h3Color: ficha.h3Color,
                acc1Detalle: ficha.acc1Detalle,
                acc1Material: ficha.acc1Material,
                acc1Color: ficha.acc1Color,
                acc2Detalle: ficha.acc2Detalle,
                acc2Material: ficha.acc2Material,
                acc2Color: ficha.acc2Color,
                acc3Detalle: ficha.acc3Detalle,
                acc3Material: ficha.acc3Material,
                acc3Color: ficha.acc3Color,
                cierreDetalle: ficha.cierreDetalle,
                cierreMaterial: ficha.cierreMaterial,
                cierreColor: ficha.cierreColor,
                pullerDetalle: ficha.pullerDetalle,
                pullerMaterial: ficha.pullerMaterial,
                pullerColor: ficha.pullerColor,
                punteraMaterial: ficha.punteraMaterial,
                contraMaterial: ficha.contraMaterial,
                p1Material: ficha.p1Material,
                p1Color: ficha.p1Color,
                p1Forrado: ficha.p1Forrado,
                p2Material: ficha.p2Material,
                p2Color: ficha.p2Color,
                p2Forrado: ficha.p2Forrado,
                plataformaMaterial: ficha.plataformaMaterial,
                plataformaColor: ficha.plataformaColor,
                plataformaForrado: ficha.plataformaForrado,
                tacoMaterial: ficha.tacoMaterial,
                tacoColor: ficha.tacoColor,
                tacoForrado: ficha.tacoForrado,
                plantillaMaterial: ficha.plantillaMaterial,
                plantillaColor: ficha.plantillaColor,
                acolcheMaterial: ficha.acolcheMaterial,
                h4Tipo: ficha.h4Tipo,
                h4Numero: ficha.h4Numero,
                h4Color: ficha.h4Color,
                costuraTipo: ficha.costuraTipo,
                selloMarca: ficha.selloMarca,
                selloTipo: ficha.selloTipo,
                selloMaterial: ficha.selloMaterial,
                cueritoMarca: ficha.cueritoMarca,
                cueritoTipo: ficha.cueritoTipo,
                cueritoMaterial: ficha.cueritoMaterial,
                hantagMarca: ficha.hantagMarca,
                img1: ficha.img1,
                img2: ficha.img2,
                img3: ficha.img3,
                img4: ficha.img4
            }})
    }

    const save = async()=>{
        const formdata = new FormData();
        formdata.append("file", file)
        formdata.append("file", file2)
        formdata.append("file", file3)
        formdata.append("file", file4)
        formdata.append("id", id)
        console.log(formdata.get(file))
        await axios({
            method: 'post',
            url:`http://192.168.0.84:8081/api/ficha/upload`,
            data: formdata
          }).then(response=>{
            console.log(response.data);
            setFichae([])
            })
        }

        return(
            <div>
                <div>
                    <form id="ficha-form">
                        <div id="general" class=""><br/><br/>
                            <div id="formGeneral">
                                <div id="divGeneral" class="row form-group">
                                    <div class="row orm-group" style = {{marginBottom: '15px'}}>
                                        <div class="col">
                                        <label class="col control-label" style={{marginLeft: '320px'}}>ID: </label>
                                            <label class="text-center" style= {{border: '2px solid #5E5446',  borderRadius:'8px', marginLeft: '30px', width: '120px' }} id="linea" type="text"  
                                                 >{state.ficha.fichaId}</label>
                                    </div>
                                    </div>
                                    <div class="col">
                                        <div class="row orm-group">
                                            <label class="col control-label">Linea:</label>
                                            <div class="col">
                                                <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.linea} 
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row form-group py-2">
                                            <label class="col control-label pull-left" >Serie:</label>
                                            <div class="col">
                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.serie} 
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left">Altura de Taco:</label>
                                            <div class="col">
                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.alTaco} 
                                                </label>                                            
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left">Color:</label>
                                            <div class="col">
                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.color} 
                                                </label>
                                            </div>
                                        </div>                                
                                    </div>

                                    <div class="col colum2">
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left">Estilo:</label>
                                            <div class="col">
                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.estilo} 
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left">Codigo de Horma:</label>
                                            <div class="col">
                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.codHorma} 
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row form-group py-1">
                                            <label class="col control-label pull-left">Codigo de Planta:</label>
                                            <div class="col">
                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.codPlanta} 
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group py-1"></div>
                                    </div>
                                </div>
                            </div>
                        </div><br/><br/>

                        <div id="formEspecificaciones">
                            <div>
                                <div id="" class="px-5 py-4 text-center">
                                    <h2 class="py-2" style={{ color: "#5E5446" }} ><b>Especificaciones Tecnicas</b></h2>
                                </div><br/>

                                <div>
                                    <div id="Espe1" class="row container">
                                        <div id="apartado1" class="col">
                                            <div class="row">
                                                <div class="col-3">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#5E5446" }} class="control-label pull-left h5"><b>Material</b></label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left">Cuero 1</label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left">Cuero 2</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left">Cuero 3</label>
                                                    </div><br/>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#5E5446" }} class="control-label pull-left h5"><b>Forro</b></label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left">Forro 1</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left">Forro 2</label>
                                                    </div>                                                
                                                </div>

                                                <div class="col contenedor1">
                                                    <div class="form-group py-1">
                                                        <label style={{backgroundColor:'#5E5446', color:'#ffffff', marginLeft: '1px'}} class="row margenTipo text-center">
                                                            <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                        </label>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.c1Tipo} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.c1Color} 
                                                        </label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.c2Tipo} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.c2Color} 
                                                        </label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.c3Tipo} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.c3Color} 
                                                        </label>
                                                        </div>
                                                    </div><br/><br/>

                                                    <div class="row form-group py-1" style={{marginTop:'20px'}}>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.f1Tipo} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.f1Color} 
                                                        </label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.f2Tipo} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.f2Color} 
                                                        </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img1" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#5E5446', color:'#ffffff' }} class="labelImg" for="LastName">
                                                    <h5 id="codigoQR"><b>Imagen 1</b></h5>
                                                </label>
                                            </div>
                                            <div id="divImg1">
                                                        <img src={"http://192.168.0.84:8081/api/ficha/"+state.ficha.img1} height='250px' width='510px'></img>
                                                </div>  
                                            <div class="text-center">
                                                <input type="file" name="file" class="btn botonImg"  id="imgRuta1" style={{backgroundColor:'#5E5446', color:'#ffffff' }}
                                               onChange={(e)=>{
                                                   setFile(e.target.files[0])}}/>
                                            </div>                                       
                                        </div>
                                    </div><br/><br/>

                                    <div id="Espe2" class="row container">
                                        <div id="apartado2" class="col">
                                            <div id="Hilo" class="">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#5E5446" }} class="control-label pull-left h5" for="LastName"><b>Hilo</b></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Hilo 1</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Hilo 2</label>
                                                        </div>

                                                        <div id="cuero3" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Hilo 3</label>
                                                        </div>                                                                                        
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#5E5446', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Número</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.h1Tipo} 
                                                        </label>
                                                            </div>
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.h1Numero} 
                                                        </label>
                                                            </div>
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.h1Color} 
                                                        </label>
                                                            </div>
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.h2Tipo} 
                                                        </label>
                                                            </div>
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.h2Numero} 
                                                        </label>
                                                            </div>
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.h2Color} 
                                                        </label>
                                                            </div>
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.h3Tipo} 
                                                        </label>
                                                            </div>
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.h3Numero} 
                                                        </label>
                                                            </div>
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.h3Color} 
                                                        </label>
                                                            </div>
                                                        </div><br/>                      
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#5E5446" }} class="control-label pull-left h5" for="LastName"><b>Accesorios</b></label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Accesorios 1</label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Accesorios 2</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Accesorios 3</label>
                                                    </div><br/>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#5E5446" }} class="control-label pull-left h5" for="LastName"><b>Cierre</b></label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Cierre</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Puller</label>
                                                    </div>                                                
                                                </div>

                                                <div class="col contenedor1">
                                                    <div class="form-group py-1">
                                                        <label style={{backgroundColor:'#5E5446', color:'#ffffff' }} class="row margenTipo text-center">
                                                            <h5 id="codigoQR" class="col"><b>Detalle</b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                        </label>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.acc1Detalle} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.acc1Material} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.acc1Color} 
                                                        </label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.acc2Detalle} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.acc2Material} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.acc2Color} 
                                                        </label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.acc3Detalle} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.acc3Material} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.acc3Color} 
                                                        </label>
                                                        </div>
                                                    </div><br/><br/><br/>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.cierreDetalle} 
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.cierreMaterial} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.cierreColor} 
                                                        </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.pullerDetalle} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.pullerMaterial} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                            {state.ficha.pullerColor} 
                                                        </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img2" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#5E5446', color:'#ffffff' }} class="labelImg">
                                                    <h5 id="codigoQR"><b>Imagen 2</b></h5>
                                                </label>
                                            </div>
                                            <div id="divImg1">
                                                        <img src={"http://192.168.0.84:8081/api/ficha/"+state.ficha.img2} height='250px' width='510px'></img>
                                                </div>
                                            <div class="text-center">
                                            <input type="file" name="file2" class="btn botonImg"  id="imgRuta1" style={{backgroundColor:'#5E5446', color:'#ffffff' }}
                                            onChange={(e)=>{setFile2(e.target.files[0])}}/>
                                            </div>                                       
                                        </div>
                                    </div><br/><br/>

                                    <div id="Espe3" class="row container">
                                        <div id="apartado3" class="col">
                                            <div id="Hilo" class="col">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left"></label>
                                                        </div> 
                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#5E5446" }} class="control-label pull-left h5"><b>Puntera</b></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#5E5446" }} class="control-label pull-left h5"><b>Contrafuerte</b></label>
                                                        </div>                                                                                 
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#5E5446', color:'#ffffff' }} class="row margenTipo">
                                                                <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                                <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.punteraMaterial} 
                                                                </label>
                                                            </div>                                                
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.contraMaterial} 
                                                                </label>
                                                            </div>                                                       
                                                        </div><br/>                      
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-3">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#5E5446" }} class="control-label pull-left h5" for="LastName"><b>Piso</b></label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Piso 1</label>
                                                    </div>
                                                    
                                                    <div id="cuero2" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Piso 2</label>
                                                    </div>

                                                    <div id="cuero3" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Plataforma</label>
                                                    </div>

                                                    <div id="cuero" class="form-group py-1">
                                                        <label class="control-label pull-left" for="LastName">Taco</label>
                                                    </div>                                         
                                                </div>

                                                <div class="col contenedor1">
                                                    <div class="form-group py-1">
                                                        <label style={{backgroundColor:'#5E5446', color:'#ffffff' }} class="row margenTipo text-center">
                                                            <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                            <h5 id="codigoQR" class="col">Color<b></b></h5>
                                                            <h5 id="codigoQR" class="col"><b>Detalle</b></h5>
                                                        </label>
                                                    </div>
                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.p1Material} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.p1Color} 
                                                                </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.p1Forrado} 
                                                                </label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.p2Material} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.p2Color} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.p2Forrado} 
                                                        </label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.plataformaMaterial} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.plataformaColor} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.plataformaForrado} 
                                                        </label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group py-1">
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.tacoMaterial} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.tacoColor} 
                                                        </label>
                                                        </div>
                                                        <div class="col">
                                                        <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.tacoForrado} 
                                                        </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img3" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#5E5446', color:'#ffffff' }} class="labelImg" for="LastName">
                                                    <h5 id="codigoQR"><b>Imagen 3</b></h5>
                                                </label>
                                            </div>
                                            <div id="divImg1">
                                                        <img src={"http://192.168.0.84:8081/api/ficha/"+state.ficha.img3} height='250px' width='510px'></img>
                                            </div>
                                            <div class="text-center">
                                            <input type="file" name="file3" class="btn botonImg"  id="imgRuta1" style={{backgroundColor:'#5E5446', color:'#ffffff' }}
                                               onChange={(e)=>{setFile3(e.target.files[0])}} />
                                            </div>                                       
                                        </div>
                                    </div><br/><br/>

                                    <div id="Espe4" class="row container">
                                        <div id="apartado4" class="col">
                                            <div id="Plantilla" class="col">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label style={{ color: "#5E5446" }} class="control-label pull-left h5" for="LastName"><b>Plantilla</b></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Plantilla</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Acolche</label>
                                                        </div>                        
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#5E5446', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.plantillaMaterial} 
                                                        </label>
                                                            </div>
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.plantillaColor} 
                                                        </label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group py-1">
                                                            <div class="">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.acolcheMaterial} 
                                                        </label>
                                                            </div>
                                                        </div>                                                   
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="costura" class="">
                                                <div class="row">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#5E5446" }} class="control-label pull-left h5"><b>Costura Plantilla</b></label>
                                                    </div>
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left"></label>
                                                        </div>
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left">Hilo</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left">Costura</label>
                                                        </div>                                                                                      
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#5E5446', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Número</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Color</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.h4Tipo} 
                                                        </label>
                                                            </div>
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.h4Numero} 
                                                        </label>
                                                            </div>
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.h4Color} 
                                                        </label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group py-1">
                                                            <div class="">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.costuraTipo} 
                                                        </label>
                                                            </div>
                                                        </div>                      
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="Accesorios" class="">
                                                <div class="row">
                                                    <div id="cuero" class="form-group py-1">
                                                        <label style={{ color: "#5E5446" }} class="control-label pull-left h5" for="LastName"><b>Accesorios de Acabado</b></label>
                                                    </div>
                                                    <div class="col-3">
                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName"></label>
                                                        </div>

                                                        <div id="cuero" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Sello</label>
                                                        </div>
                                                        
                                                        <div id="cuero2" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Cuerito</label>
                                                        </div>

                                                        <div id="cuero3" class="form-group py-1">
                                                            <label class="control-label pull-left" for="LastName">Han Tag</label>
                                                        </div>                                                                                        
                                                    </div>

                                                    <div class="col contenedor1">
                                                        <div class="form-group py-1">
                                                            <label style={{backgroundColor:'#5E5446', color:'#ffffff' }} class="row margenTipo text-center">
                                                                <h5 id="codigoQR" class="col"><b>Marca</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Tipo</b></h5>
                                                                <h5 id="codigoQR" class="col"><b>Material</b></h5>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.h4Tipo} 
                                                        </label>
                                                            </div>
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px', fontSize:'80%' }} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.selloTipo} 
                                                        </label>
                                                            </div>
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px'}} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.selloMaterial} 
                                                        </label>
                                                            </div>
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px', fontSize:'75%'}} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.cueritoMarca} 
                                                        </label>
                                                            </div>
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px', fontSize:'80%'}} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.cueritoTipo} 
                                                        </label>
                                                            </div>
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px'}} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.cueritoMaterial} 
                                                        </label>
                                                            </div>
                                                        </div>

                                                        <div class="row form-group py-1">
                                                            <div class="col">
                                                            <label class="form-control borde text-center"  style= {{border: '2px solid #5E5446',  borderRadius:'8px'}} id="FirstName" name="FirstName" type="text">
                                                                {state.ficha.hantagMarca} 
                                                        </label>
                                                            </div>
                                                        </div><br/>                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="img4" class="col">
                                            <div>
                                                <label style={{backgroundColor:'#5E5446', color:'#ffffff' }} class="labelImg" for="LastName">
                                                    <h5 id="codigoQR"><b>Imagen 4</b></h5>
                                                </label>
                                            </div>
                                            <div id="divImg1">
                                                        <img src={"http://192.168.0.84:8081/api/ficha/"+state.ficha.img4} height='250px' width='510px'></img>
                                                </div>
                                            <div class="text-center">
                                            <input type="file" name="file" class="btn botonImg"  id="imgRuta1" style={{backgroundColor:'#5E5446', color:'#ffffff' }}
                                             onChange={(e)=>{setFile4(e.target.files[0])}}   />
                                            </div>                                                                                 
                                        </div>
                                    </div><br/>
                                </div>
                            </div>                        
                        </div>
                        <div>
                            <div id="botonesFormDiv">
                                <div  class="row text-center container">
                                    <div class="col">
                                        <Link to="/fichas">
                                            <a id="margenbotom1">
                                            <input type="button" class="btn botonesForm" style={{backgroundColor:'#696666', color:'#ffffff' }} value="Guardar y Volver" onClick={save}></input></a>
                                        </Link>
                                    </div>
                                    <div class="col">
                                        <Link to="/fichas">
                                            <a id="margenbotom2">
                                            <input  type="button" class="btn botonesForm" style={{backgroundColor:'#696666', color:'#ffffff' }} value="Cancelar"></input></a>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form><br/><br/>
                </div>
            </div>
        )
}
