import { authenticate, authFailure, authSuccess } from '../../redux/authActions';
import React,{ useState } from 'react';
import { connect } from 'react-redux';
import {userLogin} from '../../api/authenticationService';
import {Alert,Spinner} from 'react-bootstrap';
import { withRouter } from 'react-router-dom';



const RigthComponent=({loading,error,...props})=> {
    
    const [values, setValues] = useState({
        username: '',
        password: ''
        });


    const handleSubmit=(evt)=>{
        evt.preventDefault();
        props.authenticate();

        userLogin(values).then((response)=>{

          console.log("response",response);

            if(response.status===200){
                props.setUser(response.data);
                props.history.push('/home');
            }
            else{
                props.loginFailure('Something Wrong!Please Try Again'); 
            }

        }).catch((err)=>{

            if(err && err.response){
            
            switch(err.response.status){
                case 401:
                    console.log("401 status");
                    props.loginFailure("Authentication Failed.Bad Credentials");
                    break;
                default:
                    props.loginFailure('Something Wrong!Please Try Again'); 

            }

            }
            else{
                props.loginFailure('Something Wrong!Please Try Again');
            }
                

            

        });
      //console.log("Loading again",loading);

      
    }

    const handleChange = (e) => {
        e.persist();
        setValues(values => ({
        ...values,
        [e.target.name]: e.target.value
        }));
    };

    
    console.log("Loading ",loading);

        return (
            <div className="col-6 flex-fill" id="login_rigth">
                <div className="row vh-100 justify-content-center align-items-center">  
                    <div className="col-auto" >
                        <h1 className="tit" style={{paddingRight:'15px', paddingLeft:'15px'}}>Website Administrativo</h1><br/>
                        <form onSubmit={handleSubmit} noValidate={false}>
                            <div className="input-group p-2">
                                <input id="username" type="text" name="username" placeholder="Usuario" className="form-control" value={values.username} onChange={handleChange} required />
                            </div>
                                
                            <div className="input-group p-2">
                                <input id="password" type="password" name="password" placeholder="Contraseña" className="form-control" value={values.password} onChange={handleChange} required/>
                            </div><br/>
    
                            <div className="text-center">
                                <button type="submit" className="btn w-50 bo">
                                        Acceso
                                        {loading && (
                                            <Spinner
                                            as="span"
                                            animation="border"
                                            size="sm"
                                            role="status"
                                            aria-hidden="true"
                                          />
                                        )}
                                </button>
                            </div>
                        </form>
                        { error &&
                            <Alert style={{marginTop:'20px'}} variant="danger">
                                    {error}
                                </Alert>

                            }
                    </div>
                </div>
            </div>
        )   
}


  const mapStateToProps=({auth})=>{
    console.log("state",auth)
    return {
        loading:auth.loading,
        error:auth.error
    }}


    const mapDispatchToProps=(dispatch)=>{

        return {
        authenticate:()=> dispatch(authenticate()),
        setUser:(data)=> dispatch(authSuccess(data)),
        loginFailure:(message)=>dispatch(authFailure(message))
        }
    }



export default withRouter(connect(mapStateToProps,mapDispatchToProps)(RigthComponent));

