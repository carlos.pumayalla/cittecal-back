import React from 'react';
import login1 from '../../assets/img/login1.png';

const LeftComponent = () => {
    return (
        <div className="col-6" id="login_left">
            <div>
                <img src={login1} alt="Citeccal Empleados" className="img-fluid float-center vh-100"/>
            </div>
        </div>
    )
}

export default LeftComponent
